import './App.css';
import {useAuth, login, logout} from "./context/auth"

function App() {
  const {state, dispatch} = useAuth()
  return (
    <div>
      {
        state.isLoggedIn ?
        <button onClick={() => dispatch(logout())}>Logout</button> :
        <button onClick={() => dispatch(login())}>Login</button>
      }
    </div>
  );
}

export default App;
