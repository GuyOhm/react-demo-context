export type AuthAction = {type: 'LOGIN'} | {type: 'LOGOUT'}
export type AuthDispatch = (action: AuthAction) => void
export type AuthState = {isLoggedIn: boolean}

export function initAuthState(): AuthState {
  return {isLoggedIn: false}
}

export function authReducer(state: AuthState, action: AuthAction) {
  switch (action.type) {
    case 'LOGIN':
      return {isLoggedIn: true}
    case 'LOGOUT':
      return {isLoggedIn: false}
    default:
      return state
  }
}


