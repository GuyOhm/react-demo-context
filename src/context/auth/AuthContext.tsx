import {createContext, useContext, useReducer} from 'react'
import { AuthDispatch, authReducer, AuthState, initAuthState } from './authReducer'

type AuthProviderProps = {children: React.ReactNode}

const AuthContext = createContext<{state: AuthState; dispatch: AuthDispatch} | undefined>(undefined)

export function AuthContextProvider({children}: AuthProviderProps) {
  const [state, dispatch] = useReducer(authReducer, initAuthState())

  return (
    <AuthContext.Provider value={{state, dispatch}}>
      {children}
    </AuthContext.Provider>
  )
}

// Custom useContext hook
export function useAuth() {
  const context = useContext(AuthContext)
  if (context === undefined) {
    throw new Error('useAuth must be used within a AuthProvider')
  }
  return context
}