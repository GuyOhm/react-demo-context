import { AuthAction } from "./authReducer"

export function login(): AuthAction {
  return {type: 'LOGIN'}
}

export function logout(): AuthAction {
  return {type: 'LOGOUT'}
}